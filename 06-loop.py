#!/usr/bin/python

cont = 1

while cont <= 10:
    print (cont,'Hello World - While Loop!')
    cont = cont + 1
print ('\n')

for i in range(10):
    print (i,'Hello World - For Loop!')
print ('\n')

'''
NA CONDICAO ABAIXO IRA EXIBIR APENAS OS SALTOS MULTIPLOS DE 3

A SINTAXE EH A SEGUINTE:
range(start, target, count)
'''
for i in range(0, 21, 3):
    print (i,'Hello World - For Loop!')
print ('\n')