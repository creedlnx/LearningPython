#!/usr/bin/python

import _thread, time

'''
1, 1, 2, 3, 5, 8, 13 ...
'''

def fib(n):
    if n == 1 or n == 2:
        return 1
    return fib(n -1) + fib(n - 2)

def fibon(n):
    print ('Fib %d: %d' % (n, fib(n)))

def run_fibs():
    fibon(35)
    fibon(10)
    fibon(30)

def run_fibs_with_threads():
    _thread.start_new_thread(fibon, (35,))
    _thread.start_new_thread(fibon, (10,))
    _thread.start_new_thread(fibon, (30,))
    time.sleep(20)

#run_fibs()
run_fibs_with_threads()

'''
CONCLUSÃO
Com o uso de threads a função que termina primeiro já é exibida, sem precisar aguardar a execução das demais
'''
