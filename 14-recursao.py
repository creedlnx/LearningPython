#!/usr/bin/python

b = int(input("Entre com o primeiro valor: "))
e = int(input("Entre com o segundo valor: "))


'''
Este if é o caso base, ou seja, o fim da recursão.
'''

def pot(b,e):
    if e == 0:
        return 1
    return b * pot(b, e-1)

print ("O Resultado da potencia eh", pot(b,e))
