#!/usr/bin/python

'''
O `self` deve ser sempre o primeiro parametro dentro da classe.
É o nome do objeto invocado no método.
'''
class Pessoa:
    def __init__(self, nome, id):
        self.nome = nome
        self.id = id

    def getNome(self):
        return self.nome
    
    def getId(self):
        return self.id

p1 = Pessoa('Anderson', 1)
p2 = Pessoa('Rachel', 2)

print (p1.getNome(), p1.getId())
print (p2.getNome())