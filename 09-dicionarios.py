#!/usr/bin/python

dic = {'Anderson':33, 'Rachel':37, 'Juan':14, 'Lelê':10, 'Samuca':6}
print (dic)

'''
Mostra a quantidade de registros
'''
print (len(dic))

'''
Exibe o valor da chave 'Anderson'
'''
print (dic['Anderson'])

print ('\n')

'''
Dúvida!

Porque 'print (dic)' exibe chave + valor e 'for chave in dic: print chave' exibe só a chave?
'''
for key in dic:
    print (key,dic[key])
    print (key)