#!/usr/bin/python

'''
Funções anônimas ou Funções Lambda são funções que podem ser passadas como parâmetro para outras funções
'''

lista = list(range(0,100))

def dobro(x):
    return x * 2

lista_dobro = map(dobro, lista)
print (list(lista_dobro))

lista_dobro = map(lambda x: x * 2, lista)
print ('\n', list(lista_dobro))