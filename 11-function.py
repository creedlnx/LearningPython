#!/usr/bin/python

def media(n1, n2):
    return (n1 + n2) / 2

n1 = float(input('\n Entre com o primeiro valor: '))
n2 = float(input('\n Entre com o segundo valor: '))

print(media(n1, n2))