#!/usr/bin/python

listaA = []
listaB = [1, 2, 3]

listaA.append(20)
listaA.append(3.14)
listaA.append('Anderson')
listaA.append(True)

listaA.extend(listaB)

'''
A FUNCAO POP EM LISTAS REMOVE UM CONTEUDO DENTRO DA LISTA. EH NECESSARIO INFORMAR O INDEX A SER REMOVIDO
'''
listaA.pop(0)

'''
A FUNCAO REMOVE DELETA UM CONTEUDO ESPECIFICO DA LISTA. NO EXEMPLO SEGUINTE ESTOU REMOVENDO A STRING ANDERSON.
'''
listaA.remove('Anderson')

tamanhoLista = len(listaA)

print (listaA)
print (tamanhoLista)

print (listaA[0])
print (listaA[1])
'''
-1 MOSTRA O ULTIMO INDEX DA LISTA
''' 
print (listaA[-1])

print ('\n')


'''
Percorrer a lista com for
'''
for i in listaA:
    print (i)

'''
Mostra o index de um elemento da lista
'''
print ('\n',listaA.index(3.14))
