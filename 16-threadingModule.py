#!/usr/bin/python

from threading import Thread
import time

def executa(i):
    print ('Executando thread %d ...' % i)
    time.sleep(3)
    print ('Finalizando a thread %d ...' % i)

for i in range(5):
    t = Thread(target=executa, args=(i,))
    t.start()

'''
Observar que as threads podem não ser finalizadas na mesma sequencia.
'''