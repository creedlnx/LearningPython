#!/usr/bin/python

s = {1,1,1,1}
print (s)

lista = [1,2,3,4,4]

s = set(lista)
print (s)

s1 = {1,2,3}
s2 = {2,3,4}

print (s1.union(s2))
print (s1.intersection(s2))
print (s1.difference(s2))