#!/usr/bin/python
import math

'''
FUNCAO MATEMATICA RETORNANDO O VALOR DE pi
'''
print ('\n ########## FUNCAO MATEMATICA RETORNANDO O VALOR DE pi ##########\n')

print ('\nDeclarando a variavel e chamando a function')
print ('pi = math.pi\n')
pi = math.pi
print ('--- O valor de pi eh',pi, '\n')

'''
OPERADORES ARITIMETICOS
'''
print (' ########## OPERADORES ARITIMETICOS ##########\n')

print ('Declarando a variavel n1')
print ('n1 = 10\n')
n1 = 10
print ('Declarando a variavel n2')
print ('n2 = 20\n')
n2 = 20
print ('Declarando a variavel n3')
print ('n3 = 4\n')
n3 = 4
print ('Declarando a variavel p1')
print ('p1 = 2\n')
p1 = 2
print ('Declarando a variavel p3')
print ('p2 = 3\n')
p2 = 3


print ('Declarando a variavel soma que ira somar os valores das variaveis n1 + n2')
print ('soma = n1 + n2\n')
soma = n1 + n2
print ('--- O resultado da soma eh',soma, '\n')

print ('Declarando a variavel sub que ira subtrair os valores das variaveis n2 - n1')
print ('sub = n2 - n1\n')
sub = n2 - n1
print ('--- O resultado da subtracao eh',sub, '\n')

print ('Declarando a variavel multi que ira multiplicar os valores das variaveis n1 x n2')
print ('multi = n1 * n2\n')
multi = n1 * n2
print ('--- O resultado da multiplacao eh',multi, '\n')

print ('Declarando a variavel div que ira dividir os valores das variaveis n2 / n1')
print ('div = n2 / n1\n')
div = n2 / n1
print ('--- O resultado da divisao eh',div, '\n')

print ('Declarando a variavel resto que ira exibir apena o resto da divisao das variaveis n1 % n3')
print ('resto = n1 % n3\n')
resto = n1 % n3
print ('--- O resto desta divisao eh',resto, '\n')

print ('Declarando a variavel pot que ira calcular a potenciacao das variaveis p1 ** p2')
print ('pot = p1 ** p2\n')
pot = p1 ** p2
print ('--- O resultado da potenciacao eh',pot, '\n')

'''
OPERADORES COMPARACAO
'''
print (' ########## OPERADORES COMPARACAO ########## \n')

print ('Declarando a variavel c1')
print ('c1 = 10\n')
c1 = 10
print ('Declarando a variavel c2')
print ('c2 = 10\n')
c2 = 10

print ('Declarando a variavel isEqualTo que ira verificar se c1 é igual a c2')
print ('isEqualTo = [c1 == c2]\n')
isEqualTo = [c1 == c2]
print ('--- A validacao se c1 eh igual a c2 eh', isEqualTo, '\n')

print ('Declarando a variavel isGreaterThan que ira verificar se c1 eh maior que c2')
print ('isGreaterThan = [c1 > c2]\n')
isGreaterThan = [c1 > c2]
print ('--- A validacao se c1 eh maior que c2 eh', isGreaterThan, '\n')

print ('Declarando a variavel isLessThan que ira verificar se c1 eh menor que c2')
print ('isLessThan = [c1 < c2]\n')
isLessThan = [c1 < c2]
print ('--- A validacao se c1 eh menor que c2 eh', isLessThan, '\n')

print ('Declarando a variavel isGreaterThanOrEqualTo que ira verificar se c1 eh maior ou igual que c2')
print ('isGreaterThanOrEqualTo = [c1 <= c2]\n')
isGreaterThanOrEqualTo = [c1 <= c2]
print ('--- A validacao se c1 eh maior ou igual que c2 eh', isGreaterThanOrEqualTo, '\n')

print ('Declarando a variavel isLessThanOrEqualTo que ira verificar se c1 eh menor ou igual que c2')
print ('isLessThanOrEqualTo = [c1 >= c2]\n')
isLessThanOrEqualTo = [c1 >= c2]
print ('--- A validacao se c1 eh menor ou igual que c2 eh', isLessThanOrEqualTo, '\n')

print ('Declarando a variavel isNotEqualTo que ira verificar se c1 eh diferente de c2')
print ('isNotEqualTo = [c1 != c2]\n')
isNotEqualTo = [c1 != c2]
print ('--- A validacao se c1 eh diferente c2 eh', isNotEqualTo, '\n')

'''
OPERADORES LOGICOS
'''
valor = True
not valor

v1 = True
v2 = False

v1 and v2

v1 or v2

not v1
