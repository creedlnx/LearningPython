#!/usr/bin/python

listaA = [0,1,2,3]

'''
Troca o valor do index 0 por 4
'''
listaA[0] = 4

print (listaA)

'''
A tupla é imutável
'''
tupla = (10,True,'Anderson',3.14)

print (tupla)
